const path = require("path");
const glob = require("glob");

module.exports = {
	entry: () => {
		return glob.sync("./public/js/src/*.js").reduce((pre, cur) => {
			pre[cur.replace(/^.*[\\\/]/, "").split(".")[0]] = cur;
			return pre;
		}, {});
	},
	output: {
		path: path.join(__dirname, "public", "js", "build"),
		filename: "[name].js",
	},
};
