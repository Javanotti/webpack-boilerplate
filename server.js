const Express = require("express");
const app = new Express();
const ExpressHandlebars = require("express-handlebars");

app.engine("hbs", ExpressHandlebars());
app.set("view engine", "hbs");

app.use(Express.static("public"));

app.listen(8000, () => console.log("Server up on 8000."));

app.get("/", async (req, res) => {
	return res.render("page1.hbs");
});
